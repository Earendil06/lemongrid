FROM google/dart-runtime as build

WORKDIR /app

ADD pubspec.* /app/
RUN pub get
ADD . /app
RUN pub get --offline
RUN dart2js -O2 -o web/main.dart.js web/main.dart

FROM nginx:alpine as deploy
WORKDIR /usr/share/nginx/html
COPY --from=build /app/web .
