import 'dart:html' as html;

class Utils {
  static var debugBox = html.querySelector('#debugBox');
  static var isVisible = true;
  static var data = {};

  static void toggleShow() {
    debugBox.style.display = isVisible ? "none" : "block";
    isVisible != isVisible;
  }

  static void log(String key, String text) {
    data[key] = text;
    _update();
  }

  static void remove(String key) {
    data.remove(key);
    _update();
  }

  static void _update() {
    debugBox.text = data.values.reduce((v, s) => v + "\n" + s);
  }

}