import 'dart:html' as html;
import 'dart:math' as math;

import 'package:stagexl/stagexl.dart';

import 'Utils.dart';
import 'World.dart';

Function mouseMove(Grid w) {
  return (MouseEvent event) {
    if (isRightPressed) {
      event.preventDefault();
      var mouseX = event.localX;
      var mouseY = event.localY;
      var dx = (mouseX * w.scale) - (w.eventOriginX * w.scale);
      var dy = (mouseY * w.scale) - (w.eventOriginY * w.scale);

      var clone = w.sprite.transformationMatrix.clone()..translate(dx, dy);
      var spaceWidth = w.stageWidth / 2;
      var spaceHeight = w.stageHeight / 2;

      var upBondOK = clone.ty <= w.scale * spaceHeight;

      var downBondOK =
          -clone.ty <= w.scale * (w.tileHeight * w.nVertical) - spaceHeight;

      var leftBondOK = clone.tx <= w.scale * spaceWidth;

      var rightBondOK =
          -clone.tx <= w.scale * (w.tileWidth * w.nHorizontal) - spaceWidth;

      /*if (upBondOK && downBondOK && leftBondOK && rightBondOK) {
        w.sprite.transformationMatrix.translate(dx, dy);
      } else {
        w.eventOriginX = event.localX;
        w.eventOriginY = event.localY;
      }*/
      w.sprite.transformationMatrix.translate(dx, dy);
    }
  };
}

Function mouseWheel(Grid w) {
  return (MouseEvent event) {
    event.preventDefault();
    var wheel = event.deltaY < 0 ? 1 : -1;
    var zoom = math.exp(wheel * w.zoomIntensity);

    var tooFarHorizontal =
        (w.scale * zoom) * w.tileWidth * w.nHorizontal < w.stageWidth * 1;
    var tooFarVertical =
        (w.scale * zoom) * w.tileHeight * w.nVertical < w.stageHeight * 1;
    /*if (w.scale * zoom <= 1 && (!tooFarHorizontal || !tooFarVertical)) {
      w.sprite.transformationMatrix.scale(zoom, zoom);
      w.scale *= zoom;
    }*/
    w.sprite.transformationMatrix.scale(zoom, zoom);
    w.scale *= zoom;

    Utils.log("scale", "scale: " + w.scale.toStringAsFixed(3));
    stage.invalidate();
  };
}

Function mouseDown(Grid w) {
  return (MouseEvent event) {
    event.preventDefault();
    w.eventOriginX = event.localX;
    w.eventOriginY = event.localY;
    isRightPressed = true;
    Utils.log(
        "clickPosition",
        "x: " +
            w.eventOriginX.toStringAsFixed(0) +
            ", y: " +
            w.eventOriginY.toStringAsFixed(0));
  };
}

bool isRightPressed = false;

Function init(BitmapData map) {
  return (
      {num nHorizontal,
      num nVertical,
      num tileWidth = 50,
      num tileHeight = 50,
      String gridType = "square"}) {
    Grid w = Grid(map,
        nHorizontal: nHorizontal,
        nVertical: nVertical,
        tileWidth: tileWidth,
        tileHeight: tileHeight,
        stageWidth: map.width,
        stageHeight: map.height,
        gridType: gridType);
    w.sprite.onMouseMove.listen(mouseMove(w));
    w.sprite.onMouseRightDown.listen(mouseDown(w));
    w.sprite.onMouseUp.listen((e) {
      e.preventDefault();
      isRightPressed = false;
    });
    w.sprite.onMouseRightUp.listen((e) {
      e.preventDefault();
      isRightPressed = false;
    });
    w.sprite.onMouseWheel.listen(mouseWheel(w));
    return w;
  };
}

var cmd = html.querySelector("#cmd") as html.InputElement;

parse(String text, String regex, Function result) {
  RegExp regexp = RegExp(regex);
  var matches = regexp.allMatches(text);
  if (matches.isNotEmpty) {
    var match = matches.elementAt(0);
    var param = match.group(2);
    print(param);
    grid = result(param);
    stage.removeChild(stage.getChildByName('grid'));
    stage.addChild(grid.sprite);
  }
}

enterCmd(html.KeyboardEvent keyEvent) {
  if (keyEvent.keyCode == html.KeyCode.ENTER) {
    var text = cmd.value;
    parse(text, r'(^ts\s+)(\d+)',
        (param) => initWithResources(tileWidth: param, tileHeight: int.parse(param)));

    parse(
        text, r'(^vn\s+)(\d+)', (param) => initWithResources(nVertical: int.parse(param)));

    parse(text, r'(^hn\s+)(\d+)',
        (param) => initWithResources(nHorizontal: int.parse(param)));

    parse(text, r'(^type\s+)(\w+)',
            (param) => initWithResources(gridType: param)); //todo copy operator for world like scala case class

    Utils.log("hn", "hn: ${grid.nHorizontal}");
    Utils.log("vn", "vn: ${grid.nVertical}");
    Utils.log("ts", "ts: ${grid.tileWidth}");
  }
}

var initWithResources;
Stage stage;
Grid grid;

num mapEventOriginX;
num mapEventOriginY;
main() async { //todo scroll the world like roll20 maybe
  cmd.onKeyUp.listen(enterCmd);

  var resourceManager = ResourceManager()
    ..addBitmapData('dungeon', 'images/testDungeon.jpg');
  await resourceManager.load();
  var dungeon = resourceManager.getBitmapData("dungeon");
  Sprite map = Sprite()..addChild(Bitmap(dungeon)); //todo map object
  map.onMouseUp.listen((e) {
    e.preventDefault();
    isRightPressed = false;
  });
  map.onMouseRightDown.listen((event) {
    event.preventDefault();
    isRightPressed = true;
    mapEventOriginX = event.localX;
    mapEventOriginY = event.localY;
  });
  map.onMouseRightUp.listen((e) {
    e.preventDefault();
    isRightPressed = false;
  });
  map.onMouseMove.listen((event) {
    event.preventDefault();
    if (isRightPressed) {
      var mouseX = event.localX;
      var mouseY = event.localY;
      var dx = mouseX - mapEventOriginX;
      var dy = mouseY - mapEventOriginY;
      map.transformationMatrix.translate(dx, dy);
    }
  });

  var options = StageOptions()
    ..antialias = true
    ..transparent = true;
  stage = Stage(html.querySelector('#stage'), options: options)
    ..backgroundColor = Color.BurlyWood
    ..scaleMode = StageScaleMode.NO_SCALE
    ..align = StageAlign.NONE;

  initWithResources = init(dungeon);
  grid = initWithResources() as Grid;
  stage.addChild(map);
  stage.addChild(grid.sprite);
  RenderLoop()..addStage(stage);

  Utils.log("hn", "hn: ${grid.nHorizontal}");
  Utils.log("vn", "vn: ${grid.nVertical}");
  Utils.log("ts", "ts: ${grid.tileWidth}");
}
