import 'package:stagexl/stagexl.dart';

class Grid {
  num nHorizontal,
      nVertical,
      zoomIntensity,
      stageWidth,
      stageHeight,
      eventOriginX,
      eventOriginY,
      scale,
      tileWidth,
      tileHeight;
  Stage stage;
  List<Tile> tiles;
  Sprite sprite;
  Bitmap map;
  String gridType;

  Grid(BitmapData map,
      {this.nHorizontal,
      this.nVertical,
      this.zoomIntensity = 0.05,
      this.stageWidth = 800,
      this.stageHeight = 800,
      this.tileWidth = 50,
      this.tileHeight = 50,
      this.scale = 1,
      this.gridType}) {
    if (this.nHorizontal == null) {
      this.nHorizontal = (stageWidth / tileWidth).truncate();
    }
    if (this.nVertical == null) {
      this.nVertical = (stageHeight / tileHeight).truncate();
    }
    this.map = Bitmap(map);
    this.eventOriginX = 0;
    this.eventOriginY = 0;
    this.sprite = this.initGrid(stageWidth, stageHeight);
    this.tiles = createTiles(tileWidth, tileHeight, nHorizontal, nVertical);
    this.tiles.forEach((tile) => this.sprite.addChild(tile));
  }

  Sprite initGrid(int width, int height) {
    Sprite grid = Sprite()
      ..name = "grid";
    return grid;
  }

  List<Tile> createTiles(
      int tileWidth, int tileHeight, int nHorizontal, int nVertical) {
    List<Tile> list = [];
    for (var i = 0; i < nHorizontal; i++) {
      for (var j = 0; j < nVertical; j++) {
        list.add(Tile(i, j, tileWidth, tileHeight, gridType));
      }
    }
    return list;
  }
}

class Tile extends Sprite {
  double strokeSize;
  int color = Color.Transparent;
  String type = "square";

  void draw(num x, num y, num width, num height, String type, int fill,
      double sSize) {
    var side = width > height ? width : height;

    if (type == 'square') {
      graphics
        ..beginPath()
//        ..rect(x * width, y * height, width, height)
        ..rect(x * side, y * side, side, side)
        ..closePath()
        ..fillColor(fill)
        ..strokeColor(Color.Black, sSize);
    } else {
      var startX = x * side - 0.5 * side * (y % 2);
      var startY = y * side - y * side / 4;
      graphics
        ..moveTo(startX, startY)
        ..beginPath()
        ..lineTo(startX - 0.5 * side, startY - 0.25 * side)
        ..lineTo(startX - 0.5 * side, startY - 0.75 * side)
        ..lineTo(startX, startY - side)
        ..lineTo(startX + 0.5 * side, startY - 0.75 * side)
        ..lineTo(startX + 0.5 * side, startY - 0.25 * side)
        ..lineTo(startX, startY)
        ..closePath()
        ..fillColor(fill)
        ..strokeColor(Color.Black, sSize);
    }
  }

  Tile(num x, num y, num width, num height, [this.type = 'square']) {
    strokeSize = width / 30;
    draw(x, y, width, height, type, color, strokeSize);
    addEventListener(MouseEvent.CLICK, (e) {
      color = color == Color.Transparent ? Color.BlueViolet : Color.Transparent;
      strokeSize = width / 30;
      graphics.clear();
      draw(x, y, width, height, type, color, strokeSize);
    });
  }
}
